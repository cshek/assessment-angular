import { Component, ViewEncapsulation } from '@angular/core';
import { StarwarsService } from './starwars.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  // Declare models for this component
  choices:Object[] = [{cat:'people'},{cat:'planets'},{cat:'vehicles'},{cat:'species'},{cat:'starships'}];
  whichCategory:string = 'people';
  model; 
  frmNumber:number = 1;

  // Declare functions
  constructor(private starwarsService:StarwarsService) {}

  // Call a method from the service
  handleClick()
  {
    this.model = null;
    this.starwarsService.getData(this.frmNumber, this.whichCategory)
    .subscribe((result)=>{this.model=result;
      console.log(this.model)});
  }

}
