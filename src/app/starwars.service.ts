import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StarwarsService {

  // Url used to access api
  apiUrl:string = 'https://swapi.co/api/'

  constructor(private http:HttpClient) { }

  getData(number, category)
  {
    return this.http.get(`${this.apiUrl}${category}/${number}`)
    .pipe(catchError(this.handleError<Object[]>('getData', [])));
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by a throw error.
      return throwError('Something bad happened; please try again later');
    };
  }
}
